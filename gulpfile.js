'use strict';

var gulp = require('gulp');
var del = require('del');
var babel = require('gulp-babel');
var runSequence = require('run-sequence');
var zip = require('gulp-zip');
var install = require("gulp-install");

gulp.task('clean', function() {
  return del('./dist');
});

gulp.task('compile', function() {
  return gulp.src('app/**/*')
    .pipe(babel())
    .pipe(gulp.dest('dist/'));
});

gulp.task('copy_config', function() {
  return gulp.src('config/**/*')
    .pipe(gulp.dest('dist/config'));
});

gulp.task('copy_appspec', function() {
  return gulp.src(['package.json', '.babelrc', 'app/definitions.yml'])
    .pipe(gulp.dest('dist/'));
});

gulp.task('packages', function() {
  return gulp.src(['package.json'])
    .pipe(gulp.dest('dist/'))
    .pipe(install({ production: true }));
});

gulp.task('build', function(callback) {
  return runSequence(
    ['clean'],
    ['compile'],
    ['copy_config'],
    ['copy_appspec'],
    ['packages'],
    callback
  );
});

gulp.task('zip', function() {
  return gulp.src('dist/**/*')
    .pipe(zip('deploy.zip'))
    .pipe(gulp.dest('./'));
});

gulp.task('deploy', function(callback) {
  return runSequence(
    ['build'],
    ['zip'],
    callback
  );
});
