user www-data;

# grep processor /proc/cpuinfo | wc -l
worker_processes auto;

error_log /var/log/nginx/error.log;
pid /var/run/nginx.pid;

include /usr/share/nginx/modules/*.conf;

events {
  # ulimit -n
  worker_connections 1024;
  use epoll;
  multi_accept on;
}

http {
  keepalive_timeout   65;

  # Allows the server to close the connection after a client stops responding.
  reset_timedout_connection on;

  sendfile            on;
  tcp_nopush          on;
  tcp_nodelay         on;
  types_hash_max_size 2048;

  include           /etc/nginx/mime.types;
  default_type      application/octet-stream;

	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;

  server_tokens off;

  server {
    listen       80;
    server_name  _;

    client_max_body_size 20M;
    client_header_buffer_size 1k;
    add_header "X-UA-Compatible" "IE=Edge,chrome=1";

    location /api {
      root /srv/api;
      proxy_pass http://127.0.0.1:3000/api;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection 'upgrade';
      proxy_set_header Host $host;
      proxy_cache_bypass $http_upgrade;
    }

    location /admin {
      root /srv/admin;
    }

    location / {
      root /srv/web;
    }
  }
}
