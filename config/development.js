'use strict';

module.exports = {
  logger: {
    level: 'DEBUG',
    console: {
      enabled: true,
      colorize: true,
      timestamp: false,
      prettyPrint: true,
      showLevel: true,
      handleExceptions: true,
      exitOnError: false
    },
    file: {
      enabled: false
    }
  },

  // @see https://mailcatcher.me/
  smtp: {
    host: '127.0.01',
    port: 1025,
    secure: false,
    auth: {
      user: '',
      pass: ''
    }
  },

  // @see: http://passportjs.org/
  // @see: https://www.npmjs.com/package/passport-facebook-token
  // @see: https://www.npmjs.com/package/passport-twitter-token
  socialKeys: {
    facebook: {
      APP_ID: '255485161529286',
      APP_SECRET: '1afe004c7a82891bcceeadb505fb0704'
    },
    twitter: {
      CONSUMER_KEY: 'Wd2AGrEb9jnBjHTdT83ozINZo',
      CONSUMER_SECRET: 'T1nQzci2lsIlfNRpqdBXHOfd8l4FLhzHxbJIQjwfJZ3vhEVQ0S'
    }
  }
};
