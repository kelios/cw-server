'use strict';

module.exports = {
  // Listen on this port
  port: process.env.PORT || 3000,

  // @see: https://www.npmjs.com/package/cors
  cors: {},

  // Pagination limits
  defaultLimit: 25,
  maxLimit: 100,

  // Configure session token generation.
  // Define duration and algorithm.
  session: {
    secret: process.env.SECRET_KEY || 'BF847BBE4754EC171812E54831873',
    algorithm: 'HS256',
    expire: 60 * 60 * 24 * 365
  },

  logger: {
    level: 'INFO',
    console: {
      enabled: false
    },
    file: {
      enabled: false
    }
  },

  // @see: https://www.npmjs.com/package/express-jwt
  firewall: {
    anonymous: [
      // Documentation
      /^\/api\/docs\/*/,

      // Allow to create new sessions
      /^\/api\/sessions\/*/,

      // Signup
      /^\/api\/users\/?$/,

      // Allow to confirm the user without authentication
      /^\/api\/users\/[a-zA-Z0-9]+\/confirmations\/[a-zA-Z0-9]+/,

      // Zones
      /^\/api\/zones\/?$/,

      // Teams
      /^\/api\/teams\/?$/
    ]
  },

  // @see: http://mongodb.github.io/node-mongodb-native/
  mongodb: {
    dsn: 'mongodb://127.0.0.1:27017/hippo',
    options: {
      promiseLibrary: require('bluebird')
    }
  },

  // Store HTML messages.
  templatesDir: __dirname + '/templates',

  // @see: https://nodemailer.com/
  smtp: {
    host: '127.0.01',
    port: 25,
    secure: false,
    auth: {
      user: '',
      pass: ''
    }
  },

  // @see: http://passportjs.org/
  // @see: https://www.npmjs.com/package/passport-facebook-token
  // @see: https://www.npmjs.com/package/passport-twitter-token
  socialKeys: {
    facebook: {
      APP_ID: '',
      APP_SECRET: ''
    },
    twitter: {
      CONSUMER_KEY: '',
      CONSUMER_SECRET: ''
    }
  }
};
