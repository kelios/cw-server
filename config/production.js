'use strict';

module.exports = {
  logger: {
    level: 'INFO',
    console: {
      enabled: false
    },
    file: {
      enabled: true,
      filename: __dirname + '/../logs/production.log',
      timestamp: true,
      maxsize: 500 * 1024 * 1000, // 500MB in bytes
      json: false,
      showLevel: true,
      handleExceptions: true,
      exitOnError: false
    }
  },

  // @see https://mailcatcher.me/
  smtp: {
    // Don't send any message.
    // @see https://nodemailer.com/transports/stream/
    jsonTransport: true
  },

  // @see: http://mongodb.github.io/node-mongodb-native/
  mongodb: {
    dsn: 'mongodb://hippo:*7!C9~f+$W~uD~>v@192.168.165.132:27017/hippo',
    options: {
      promiseLibrary: require('bluebird')
    }
  },

  // @see: http://passportjs.org/
  // @see: https://www.npmjs.com/package/passport-facebook-token
  // @see: https://www.npmjs.com/package/passport-twitter-token
  socialKeys: {
    facebook: {
      APP_ID: '255485161529286',
      APP_SECRET: '1afe004c7a82891bcceeadb505fb0704'
    },
    twitter: {
      CONSUMER_KEY: 'Wd2AGrEb9jnBjHTdT83ozINZo',
      CONSUMER_SECRET: 'T1nQzci2lsIlfNRpqdBXHOfd8l4FLhzHxbJIQjwfJZ3vhEVQ0S'
    }
  }
};
