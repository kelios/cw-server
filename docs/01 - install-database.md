# Server installation

## Database node

### Disk
This Linode has a disk of 48GB SSDD partitioned into two disks. One of 5GB for the operating system and packages installation,
the second to store data with a capacity of 43GB.

The second disk should be configured inside the profile so it can be recognized by the operating system. The steps are as following:

 1. Inside the Database Linode (Dashboard).
 2. Click link My Ubuntu 16.04 LTS Profile.
 3. In the group *Block Device Assignment* select data from the list of option */dev/sdc*.

Boot the Linode as normal.

### Mount disk automatically
Check that the disk is available in the system:

```
sudo lsblk -o NAME,FSTYPE,SIZE,MOUNTPOINT,LABEL
```

The output will be similar to:

```
NAME FSTYPE  SIZE MOUNTPOINT LABEL
sdb  swap    256M [SWAP]
sdc  ext4   42.8G
sda  ext4      5G /
```

Create a mounting point

```
mkdir /media/data
```

Look for the UUID of the unmounted partition:

```
sudo blkid -o list
> 52a829cb-cb49-4068-adc4-dc77e57fbd7b
```

Mount permanently:

```
sudo nano /etc/fstab
UUID=<UUID>   /media/data     ext4   defaults,user 0 2
```

**Reboot**.

### MongoDB Installation
#### Official repository

```
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6

echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list

sudo apt-get update
sudo apt-get install -y mongodb-org
```

### System service
Install daemon as a system service so it can be started, stopped or restarted.

```
sudo nano /etc/systemd/system/mongodb.service
```

Paste

```
[Unit]
Description=High-performance, schema-free document-oriented database
After=network.target

[Service]
User=mongodb
ExecStart=/usr/bin/mongod --quiet --config /etc/mongod.conf

[Install]
WantedBy=multi-user.target
```

#### Start the service

```
sudo systemctl enable mongodb.service
sudo systemctl start mongodb.service
sudo systemctl status mongodb.service
```

### Configure database

#### Save data in separate disk

```
mkdir -p /media/data/mongodb/data
mkdir -p /media/data/mongodb/logs
sudo chmod 777 -R /media/data/mongodb
```

#### Configuration

```
sudo nano /etc/mongod.conf
```

Paste the following configuration:

```
storage:
  dbPath: /media/data/mongodb/data
  journal:
    enabled: true

systemLog:
  destination: file
  logAppend: true
  path: /media/data/mongodb/logs/mongod.log

net:
  port: 27017
  bindIp: 192.168.165.132

  http:
    enabled: false
    JSONPEnabled: false
    RESTInterfaceEnabled: false
```

> **Note:**
> - 192.168.165.132 is the private IP.  This database is only accessible in the private network.
> - 27017 is the listening port.

Restart to apply the changes

```
sudo systemctl restart mongodb.service
sudo systemctl status mongodb.service
```

### Client access

Enter to mongo shell

```
mongo 192.168.165.132:27017
```

#### Set password to the admin user

```
use admin

db.createUser(
  {
    user: "admin",
    pwd: "6EeJdVSJ&xwqtxA3",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
  }
)

db.grantRolesToUser(
   "admin",
   [ "readWrite", { role: "root", db: "admin" } ]
)
```

#### App database

```
use hippo

db.createUser(
  {
    user: "hippo",
    pwd: "*7!C9~f+$W~uD~>v",
    roles: [ { role: "readWrite", db: "hippo" } ]
  }
)
```

### Start service with authentication

```
sudo nano /etc/mongod.conf

security:
  authorization: enabled
```

Restart to apply changes

```
sudo systemctl restart mongodb.service
```

For further shell login proceed as follows:

```
mongo 192.168.165.132:27017
db.getSiblingDB("hippo").auth("hippo", "*7!C9~f+$W~uD~>v" )
```

#### Allow connections

Allow connections from the App node in the private network

```
sudo ufw allow from 192.168.186.45/32 to any port 27017
sudo ufw reload
```
