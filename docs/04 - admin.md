# Administration

## Create admin user

Login into Database node and create the admin user as follows:

```
mongo 192.168.165.132:27017
use hippo
db.getSiblingDB("hippo").auth("hippo", "*7!C9~f+$W~uD~>v" )

db.User.insert({
   "_id" : ObjectId("5893fc60cbb1126ba67b6f35"),
   "updatedAt" : ISODate("2017-02-03T03:43:43.609Z"),
   "createdAt" : ISODate("2017-02-03T03:43:28.973Z"),
   "name" : "Admin Hippo",
   "email" : "admin@hippocarwash.cl",
   "dni" : "admin",
   "roles" : ["ROLE_ADMIN"],
   "confirmationToken" : null,
   "confirmedAt" : ISODate("2017-02-03T03:43:43.609Z"),
   "provider" : "local",
   "password" : "$2a$10$qNTVHa0W75GYVkmr5RzS5O1SxNn/WYN.HXRJYcSTv9lsClOGDHabq",
   "__v" : 0
})
```

> **Credentials:**
> - Password: adm1n+h1pp0
> - Login: admin or admin@hippocarwash.cl

Example of sessions:

```
curl -XPOST http://45.56.67.87/api/sessions --header 'Content-Type: application/json' -d'{"login": "admin", "password": "adm1n+h1pp0"}

# output:
{
    "type": "session",
    "id": "5893fc60cbb1126ba67b6f35",
    "attributes": {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU4OTNmYzYwY2JiMTEyNmJhNjdiNmYzNSIsImVtYWlsIjoiYWRtaW5AaGlwcG8uY2wiLCJyb2xlcyI6WyJST0xFX0FETUlOIl0sImlhdCI6MTQ4NjA5Mzc1NSwibmJmIjoxNDg2MDkzNjk1LCJleHAiOjE1MTc2Mjk3NTV9.Sj1WxoGS-hEf_SScCn-imaAouwu7wrvj58WdYYxwNIQ",
        "user": {
            "_id": "5893fc60cbb1126ba67b6f35",
            "updatedAt": "2017-02-03T03:43:43.609Z",
            "createdAt": "2017-02-03T03:43:28.973Z",
            "name": "Admin Hippo",
            "email": "admin@hippocarwash.cl",
            "dni": "admin",
            "roles": [
                "ROLE_ADMIN"
            ],
            "confirmationToken": null,
            "confirmedAt": "2017-02-03T03:43:43.609Z"
        }
    }
}
```
