# Server installation

## App node

### Directories

```
mkdir -p /srv/api
mkdir /srv/web
mkdir /srv/admin
```

### Nginx

```
sudo apt-get install -y nginx
```

Open ports

```
sudo ufw allow http
sudo ufw allow https
sudo ufw reload
```

Change directory permissions:

```
sudo chown -R www-data:www-data /srv/
sudo usermod -a -G www-data hippo
sudo chgrp -R www-data /srv/
sudo chmod -R g+rwx /srv/
```

### Configuration

```
sudo nano /etc/nginx/
```

Replace the content with the following configuration:

```
user www-data;
worker_processes auto;

error_log /var/log/nginx/error.log;
pid /var/run/nginx.pid;

include /usr/share/nginx/modules/*.conf;

events {
  # ulimit -n
  worker_connections 1024;
  use epoll;
  multi_accept on;
}

http {
  keepalive_timeout   65;

  # Allows the server to close the connection after a client stops responding.
  reset_timedout_connection on;

  sendfile            on;
  tcp_nopush          on;
  tcp_nodelay         on;
  types_hash_max_size 2048;

  include           /etc/nginx/mime.types;
  default_type      application/octet-stream;

  access_log /var/log/nginx/access.log;
  error_log /var/log/nginx/error.log;

  server_tokens off;

  server {
    listen       80;
    server_name  _;

    client_max_body_size 20M;
    client_header_buffer_size 1k;
    add_header "X-UA-Compatible" "IE=Edge,chrome=1";

    location /api {
      root /srv/api;
      proxy_pass http://127.0.0.1:3000/api;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection 'upgrade';
      proxy_set_header Host $host;
      proxy_cache_bypass $http_upgrade;
    }

    location /admin {
      root /srv/admin;
    }

    location / {
      root /srv/web;
    }
  }
}
``` 

Restart Nginx

```
sudo systemctl restart nginx.service
sudo systemctl status nginx.service
```

### NodeJS

#### Dependencies

```
sudo apt-get install -y software-properties-common build-essential libssl-dev python-pip curl wget unzip make
```

#### Installation

```
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs
node -v
npm -v
```

Install pm2 with the user of deploys:

```
ssh hippo@45.56.67.87
sudo npm install -g pm2
sudo env PATH=$PATH:/usr/bin pm2 startup ubuntu -u hippo --hp /home/hippo
pm2 save
```
