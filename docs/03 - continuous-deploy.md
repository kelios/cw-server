#Server installation

## Continuous deployment

The deployment process will occur over SSH. A key-pair of public/private keys will be generated and the public key copied to the App Linode. Every time a deployment is needed, an SSH connection will be made using the private key and the files will be copied using SSH, also remote commands will be executed to perform restart of services.

The deployment process will be triggered from BitBucket Pipelines as described below in this manual.

### Keys

```
ssh-keygen -t rsa -N '' -f deploy_ssh_key

# output (do not execute)
Generating public/private rsa key pair.
Your identification has been saved in deploy_ssh_key.
Your public key has been saved in deploy_ssh_key.pub.
The key fingerprint is:
SHA256:hO+uDPFjlaOtS7ccSF/y6fFIc8I5i0+pH06Dd4WfMzY alberto@alberto-GL752VW
The key's randomart image is:
+---[RSA 2048]----+
|                 |
|       .         |
|      . .        |
|       o .   .   |
|    . . S . . .  |
|     + B B + o . |
|    . B B ^ o E  |
|     = * % & . + |
|      =o*oB .    |
+----[SHA256]-----+
```

Store the private key in a secure Pipelines environment variable (*DEPLOY_SSH_KEY*). We'll base-64 encode it to make sure it survives the trip through the Pipelines UI.

```
base64 < deploy_ssh_key
```

Copy and paste the value in a new environment variable DEPLOY_SSH_KEY.
https://bitbucket.org/kelios/cw-server/admin/addon/admin/pipelines/repository-variables#!/variables

Upload the public key to the server that you want to communicate with from your pipeline.

```
ssh-copy-id -i deploy_ssh_key.pub hippo@45.56.67.87
```

To verify the correct installation, try to login into the server with the following command (no password should be asked)

```
ssh -i deploy_ssh_key hippo@45.56.67.87
```

For security reasons remove your local files because the public key is now in the server and the private key as a environment variable in BitBucket.

```
rm deploy_ssh_key
rm deploy_ssh_key.pub
```

### Allow user to restart Nginx

Is recommendable to reload Nginx after every deploy in order to refresh the changes in the directory

```
sudo nano /etc/sudoers
```

And add the following line to avoid asking password:

```
hippo ALL=NOPASSWD: /bin/systemctl reload nginx.service
```
