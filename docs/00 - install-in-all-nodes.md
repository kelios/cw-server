# Server installation

This file describe the infrastructure installation and configuration. All commands are made in Linode servers with Ubuntu 16.04 LTS as operating system.

----------

## Linodes

| Name     | Specs                           | Public IP        | Private IP         | Root             | User  | Password         |
|----------|---------------------------------|------------------|--------------------|------------------|-------|------------------|
| Database | 4 GB RAM  48 GB SSD 2 CPU Cores | 66.228.49.129/24 | 192.168.165.132/17 | nY>q2m4Ymp?BSxJQ | hippo | CLd8#eqC!KAPFeK5 |
| App      | 2 GB RAM 24 GB SSD 2 CPU Cores  | 45.56.67.87/24   | 192.168.186.45/17  | {A+$bNh$HUnSk]3% | hippo | CLd8#eqC!KAPFeK5 |


## Configure both Linodes
### Get latest version of packages

```
sudo apt-get update
sudo apt-get -y upgrade
```

### Timezone/Locale

```
sudo timedatectl set-timezone MST

echo "# Locale settings
export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8">>~/.bash_profile

source ~/.bash_profile
locale-gen en_US.UTF-8
```

### SSH access

```
adduser hippo
usermod -aG sudo hippo

sudo ufw allow OpenSSH
sudo ufw enable
```

### Allow binding on private IP

```
sudo nano /etc/network/interfaces
```

Edit to match the following configuration.

```
auto eth0 eth0:1
iface eth0:1 inet static
    address <private-ip>/17
```

Then, prevent losing this config after reboots:

 1. Inside the Linode (Dashboard).
 2. Click link My Ubuntu 16.04 LTS Profile.
 3.  In the group *Filesystem/Boot Helpers* select *NO* for *Auto-configure Networking*.

**Reboot the Linode**.
