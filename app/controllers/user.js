'use strict';

import getLogger from './../packages/logger';
import userSignupSchema from './../validation/schemas/userSignupSchema';
import passwordSchema from './../validation/schemas/passwordSchema';
import formatValidationErrors from './../validation/formatter';
import createUserAction from './../actions/createUser';
import createSessionAction from './../actions/createSession';
import confirmUserAction from './../actions/confirmUser';
import listUsersAction from './../actions/listUsersAction';
import { formatObject, formatError, formatPagination } from './../packages/formatJsonApi';

let logger = getLogger();

/**
 * Create an user account.
 * This registration is based on form.
 */
export const createUser = (req, res) => {

  logger.info('Creating account');

  // Validate data sent.
  req.checkBody(userSignupSchema);
  req.asyncValidationErrors(true).then(() => {

    // Validation pass.
    // Create the account and configure it.
    createUserAction(req.body, (err, user) => {

      if (err) {
        logger.error(err);

        return res.status(500).json(formatError({
          status: 500,
          code: 'E_SIGNUP_ERROR',
          title: 'Error creating the account.',
          detail: 'This action could not complete due internal errors.'
        }));
      }

      // Process finished successfully.
      // Generate authorization token.
      createSessionAction(user, (err, data) => {

        if (err) {
          logger.error('Error creating the session',);
          logger.error(err);

          return res.status(500).json(formatError({
            status: 500,
            code: 'E_SESSION_ERROR',
            title: 'Error creating the session.',
            detail: 'This action could not complete due internal errors.'
          }));
        }

        return res.status(201).json(formatObject(data, 'session', {id: user.id}));

      });

    });

  })
  .catch(err => {

    return res.status(400).json(formatError({
      status: 400,
      code: 'E_VALIDATION_ERROR',
      title: 'Validation error.',
      detail: 'The registration data contains errors.',
      meta: {
        errors: formatValidationErrors(err)
      }
    }));

  });

};


/**
 * List existing users.
 */
export const listUsers = (req, res) => {

  logger.info('List users');
  const filters = req.query;

  listUsersAction(filters, (err, results) => {

    if (err) {
      logger.error('Error listing users',);
      logger.error(err);

      return res.status(500).json(formatError({
        status: 500,
        code: 'E_LIST_USERS_ERROR',
        title: 'Error listing users.',
        detail: 'This action could not complete due internal errors.'
      }));
    }

    return res.status(200).json(formatPagination(results, 'user', { dropFields: ['password', 'confirmationToken'] }));

  })

};


/**
 * Confirm account.
 */
export const confirmUser = (req, res) => {

  logger.info('Confirm user');
  const token = req.params.token;

  // The user must have assigned the token.
  if (req.ruser.confirmationToken !== token) {
    return res.status(404).json({});
  }

  confirmUserAction(req.ruser, err => {

    if (err) {
      logger.error('Error confirming user');
      logger.error(err);

      return res.status(500).json(formatError({
        status: 500,
        code: 'E_CONFIRMATION_ERROR',
        title: 'Confirmation error.',
        detail: 'The user is not confirmed due to internal errors.'
      }));
    }

    return res.status(204).json({});

  });

};


/**
 * Get details about the user.
 */
export const getUser = (req, res) => {

  logger.info('Get user');

  return res.status(200).json(formatObject(req.ruser, 'user', { dropFields: ['password', 'confirmationToken'] }));

};


/**
 * Change password.
 */
export const changePassword = (req, res) => {

  logger.info('Change password');

  // Validate data sent.
  req.checkBody(passwordSchema);
  req.asyncValidationErrors(true).then(() => {

    if (req.user.id.toString() !== req.ruser.id.toString()) {
      return res.status(403).json(formatObject({
        status: 403,
        code: 'E_DENIED_ERROR',
        title: 'Operation denied',
        detail: 'You can not change password to other users'
      }));
    }

    req.ruser.password = req.body.password;
    req.ruser.save(err => {

      if (err) {
        logger.error('Error changing password');
        logger.error(err);

        return res.status(500).json(formatError({
          status: 500,
          code: 'E_CHANGE_PASSWORD_ERROR',
          title: 'Password not changed',
          detail: 'This action could not complete due internal errors.'
        }));
      }

      return res.status(202).json({});

    });

  })
  .catch(err => {

    return res.status(400).json(formatError({
      status: 400,
      code: 'E_VALIDATION_ERROR',
      title: 'Validation error.',
      detail: 'The data contains errors.',
      meta: {
        errors: formatValidationErrors(err)
      }
    }));

  });

};
