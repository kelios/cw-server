'use strict';

import mongoose from 'mongoose';
import getLogger from './../packages/logger';
import carBrandSchema from './../validation/schemas/carBrandSchema';
import formatValidationErrors from './../validation/formatter';
import * as jsonApi from './../packages/formatJsonApi';

let logger = getLogger();
let CarBrand = mongoose.model('CarBrand');

/**
 * Create a car brand
 */
export const createCarBrand = (req, res) => {

  logger.info('Creating car brand');

  req.checkBody(carBrandSchema);
  req.asyncValidationErrors(true).then(() => {

    return CarBrand.create(req.body, (err, brand) => {
      if (err) {
        logger.error('Error creating a car brand');
        logger.error(err);

        return res.status(500).json(jsonApi.formatError({
          status: 500,
          code: 'E_CREATE_CAR_BRAND_ERROR',
          title: 'Error creating the car brand.',
          detail: 'This action could not complete due internal errors.'
        }));
      }

      return res.status(201).json(jsonApi.formatObject(brand, 'carBrand'));

    });
  })
  .catch(err => {

    return res.status(400).json(jsonApi.formatError({
      status: 400,
      code: 'E_VALIDATION_ERROR',
      title: 'Validation error.',
      detail: 'The registration data contains errors.',
      meta: {
        errors: formatValidationErrors(err)
      }
    }));

  });

};


/**
 * List all car brands.
 */
export const listCarBrand = (req, res) => {

  logger.info(`List car brands`);
  CarBrand.paginate({}, {page: req.query.page, limit: req.query.limit}, (err, result) => {

    if (err) {
      logger.error('Error retrieving the list of brand');
      logger.error(err);

      return res.status(500).json(jsonApi.formatError({
        status: 500,
        code: 'E_LIST_CAR_BRAND_ERROR',
        title: 'Error listing car brands.',
        detail: 'This action could not complete due internal errors.'
      }));
    }

    return res.status(200).json(jsonApi.formatPagination(result, 'carBrand'));

  });

};


/**
 * Update an existing car brand.
 */
export const updateCarBrand = (req, res) => {

  logger.info(`Update car brand with id: ${req.carBrand.id}`);
  req.checkBody(carBrandSchema);
  req.asyncValidationErrors(true).then(() => {

    CarBrand.findByIdAndUpdate({'_id': req.carBrand.id}, req.body, {new: true}, (err, brand) => {
      if (err) {
        logger.error('Error updating a car brand');
        logger.error(err);

        return res.status(500).json(jsonApi.formatError({
          status: 500,
          code: 'E_CREATE_CAR_BRAND_ERROR',
          title: 'Error creating the car brand.',
          detail: 'This action could not complete due internal errors.'
        }));
      }

      return res.status(200).json(jsonApi.formatObject(brand, 'carBrand'));

    });
  })
    .catch(err => {

      return res.status(400).json(jsonApi.formatError({
        status: 400,
        code: 'E_VALIDATION_ERROR',
        title: 'Validation error.',
        detail: 'The registration data contains errors.',
        meta: {
          errors: formatValidationErrors(err)
        }
      }));

    });
};

/**
 * Remove an existing car brand.
 */
export const removeCarBrand = (req, res) => {
  logger.info(`Delete car brand with id: ${req.carBrand.id}`);
  CarBrand.findByIdAndRemove(req.carBrand.id, (err, carBrand) => {

    if (err) {
      logger.error(err);

      return res.status(500).json(formatError({
        status: 500,
        code: 'E_REMOVE_CAR_BRAND_ERROR',
        title: 'Error removing a car brand.',
        detail: 'This action could not complete due internal errors.'
      }));
    }

    if (!carBrand) {
      return res.status(404).json({});
    }

    return res.status(204).json({});

  });

};
