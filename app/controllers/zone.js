'use strict';

import mongoose from 'mongoose';
import getLogger from './../packages/logger';
import zoneSchema from './../validation/schemas/zoneSchema';
import formatValidationErrors from './../validation/formatter';
import createZoneAction from './../actions/createZone';
import { formatObject, formatError, formatPagination } from './../packages/formatJsonApi';

let logger = getLogger();
let Zone = mongoose.model('Zone');

/**
 * Create a zone (a.k.a comuna).
 */
export const createZone = (req, res) => {

  logger.info('Creating zone');

  // Validate data sent.
  req.checkBody(zoneSchema);
  req.asyncValidationErrors(true).then(() => {

    // Validation passed
    return createZoneAction(req.body, (err, zone) => {

      if (err) {
        return res.status(500).json(formatError({
          status: 500,
          code: 'E_CREATE_ZONE_ERROR',
          title: 'Error creating the zone.',
          detail: 'This action could not complete due internal errors.'
        }));
      }

      return res.status(201).json(formatObject(zone, 'zone'));

    });

  })
  .catch(err => {

    return res.status(400).json(formatError({
      status: 400,
      code: 'E_VALIDATION_ERROR',
      title: 'Validation error.',
      detail: 'The registration data contains errors.',
      meta: {
        errors: formatValidationErrors(err)
      }
    }));

  });

};


/**
 * List all zones created.
 */
export const listZones = (req, res) => {

  Zone.paginate({}, {}, (err, results) => {

    if (err) {
      logger.error('Error retrieving the list of zone');
      logger.error(err);

      return res.status(500).json(formatError({
        status: 500,
        code: 'E_LIST_ZONE_ERROR',
        title: 'Error listing zones.',
        detail: 'This action could not complete due internal errors.'
      }));
    }

    return res.status(200).json(formatPagination(results, 'zone'));

  });

};

/**
 * Remove an existing zone.
 */
export const removeZone = (req, res) => {

  Zone.findByIdAndRemove(req.params.zone, (err, zone) => {

    if (err) {
      logger.error(err);

      return res.status(500).json(formatError({
        status: 500,
        code: 'E_REMOVE_ZONE_ERROR',
        title: 'Error removing zone.',
        detail: 'This action could not complete due internal errors.'
      }));
    }

    if (!zone) {
      return res.status(404).json(formatError({
        status: 404,
        code: 'E_NOT_FOUND_ERROR',
        title: 'Team not found.',
        detail: 'The team was not found.'
      }));
    }

    return res.status(204).json({});

  });

};
