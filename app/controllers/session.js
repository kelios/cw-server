'use strict';

import passport from 'passport';
import createSessionAction from './../actions/createSession';
import getLogger from './../packages/logger';
import { formatObject, formatError } from './../packages/formatJsonApi';

let logger = getLogger();

const providers = {
  'local'    : 'local',
  'facebook' : 'facebook-token',
  'twitter'  : 'twitter-token'
};

/**
 * Create a session by generating new authorization token.
 */
export const createSession = (req, res, next) => {

  const provider = req.query.provider || 'local';
  logger.debug(`Authenticating with provider ${provider}`);

  passport.authenticate(providers[provider] || 'local', (err, user, info) => {

    const notAuthorizedError = formatError({
      status: 401,
      code: 'E_UNAUTHORIZED_ERROR',
      title: 'Unauthorized error.',
      detail: 'The user or its credentials are not authorized.',
      meta: info || { message: 'Unknown user' }
    });

    if (err) {
      logger.error('Error validating provider');
      logger.error(err);
    }

    if (err || !user) {
      return res.status(401).json(notAuthorizedError);
    }

    // Login the user without cookie session.
    req.logIn(user, { session: false }, err => {

      if (err) {
        logger.error(err);

        return next(err);
      }

      // Login success.
      // req.user exists after login.
      createSessionAction(req.user, (err, data) => {

        if (err) {
          logger.error('Error creating session');
          logger.error(err);

          return next(err);
        }

        return res.status(201).json(formatObject(data, 'session', {id:  user.id}));

      });

    });

  })(req, res, next);

};
