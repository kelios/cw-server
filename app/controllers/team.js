'use strict';

import mongoose from 'mongoose';
import getLogger from './../packages/logger';
import teamSchema from './../validation/schemas/teamSchema';
import formatValidationErrors from './../validation/formatter';
import createTeamAction from './../actions/createTeam';
import { formatObject, formatError, formatPagination } from './../packages/formatJsonApi';

let logger = getLogger();
let Team = mongoose.model('Team');

/**
 * Create a team.
 */
export const createTeam = (req, res) => {

  logger.info('Creating team');

  // Validate data sent.
  req.checkBody(teamSchema);
  req.asyncValidationErrors(true).then(() => {

    // Validation pass.
    createTeamAction(req.body, (err, team) => {

      if (err) {
        logger.error(err);

        return res.status(500).json(formatError({
          status: 500,
          code: 'E_CREATE_TEAM_ERROR',
          title: 'Error creating the team.',
          detail: 'This action could not complete due internal errors.'
        }));
      }

      return res.status(201).json(formatObject(team, 'team', { nested: { zone: 'zone' } }));

    });

  })
  .catch(err => {

    return res.status(400).json(formatError({
      status: 400,
      code: 'E_VALIDATION_ERROR',
      title: 'Validation error.',
      detail: 'The registration data contains errors.',
      meta: {
        errors: formatValidationErrors(err)
      }
    }));

  });

};

/**
 * List teams with pagination.
 */
export const listTeam = (req, res) => {

  logger.info('Listing teams');

  const paginationOptions = {
    page: req.query.page,
    limit: req.query.limit,
    populate: 'zone'
  };

  Team.paginate({}, paginationOptions, (err, results) => {

    if (err) {
      logger.error('Error retrieving the list of teams');
      logger.error(err);

      return res.status(500).json(formatError({
        status: 500,
        code: 'E_LIST_TEAM_ERROR',
        title: 'Error listing teams.',
        detail: 'This action could not complete due internal errors.'
      }));
    }

    return res.status(200).json(formatPagination(results, 'team', { nested: { zone: 'zone' } }));

  });

};

/**
 * Remove existing team.
 */
export const removeTeam = (req, res) => {

  logger.info('Remove a team');

  Team.findByIdAndRemove(req.params.team, (err, team) => {

    if (err) {
      logger.error(err);

      return res.status(500).json(formatError({
        status: 500,
        code: 'E_REMOVE_TEAM_ERROR',
        title: 'Error removing team.',
        detail: 'This action could not complete due internal errors.'
      }));
    }

    if (!team) {
      return res.status(404).json(formatError({
        status: 404,
        code: 'E_NOT_FOUND_ERROR',
        title: 'Team not found.',
        detail: 'The team was not found.'
      }));
    }

    return res.status(204).json({});

  });

};
