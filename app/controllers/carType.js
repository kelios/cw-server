'use strict';

import mongoose from 'mongoose';
import getLogger from './../packages/logger';
import carTypeSchema from './../validation/schemas/carTypeSchema';
import formatValidationErrors from './../validation/formatter';
import * as jsonApi from './../packages/formatJsonApi';

let logger = getLogger();
let CarType = mongoose.model('CarType');

/**
 * Create a car type
 */
export const createCarType = (req, res) => {

  logger.info('Creating car type');

  req.checkBody(carTypeSchema);
  req.asyncValidationErrors(true).then(() => {

    return CarType.create(req.body, (err, type) => {
      if (err) {
        logger.error('Error creating a card type');
        logger.error(err);

        return res.status(500).json(jsonApi.formatError({
          status: 500,
          code: 'E_CREATE_CAR_TYPE_ERROR',
          title: 'Error creating the car type.',
          detail: 'This action could not complete due internal errors.'
        }));
      }

      return res.status(201).json(jsonApi.formatObject(type, 'carType'));

    });
  })
  .catch(err => {

    return res.status(400).json(jsonApi.formatError({
      status: 400,
      code: 'E_VALIDATION_ERROR',
      title: 'Validation error.',
      detail: 'The registration data contains errors.',
      meta: {
        errors: formatValidationErrors(err)
      }
    }));

  });

};


/**
 * List all car types.
 */
export const listCarType = (req, res) => {

  logger.info('List car type');

  const paginationOptions = {
    page: req.query.page,
    limit: req.query.limit,
    populate: CarType.defaultPopulateOptions()
  };

  CarType.paginate({}, paginationOptions, (err, result) => {

    if (err) {
      logger.error('Error retrieving the list of type');
      logger.error(err);

      return res.status(500).json(jsonApi.formatError({
        status: 500,
        code: 'E_LIST_CAR_TYPE_ERROR',
        title: 'Error listing car brands.',
        detail: 'This action could not complete due internal errors.'
      }));
    }
    return res.status(200).json(jsonApi.formatPagination(result, 'carType'));
  });

};


/**
 * Update an existing car type.
 */
export const updateCarType = (req, res) => {

  logger.info(`Update car type with id: ${req.params.id}`);
  req.checkBody(carTypeSchema);
  req.asyncValidationErrors(true).then(() => {

    CarType.findByIdAndUpdate(req.params.id, req.body, {new: true}, (err, type) => {
      if (err) {
        logger.error('Error updating a card type');
        logger.error(err);

        return res.status(500).json(jsonApi.formatError({
          status: 500,
          code: 'E_CREATE_CAR_TYPE_ERROR',
          title: 'Error creating the car type.',
          detail: 'This action could not complete due internal errors.'
        }));
      }

      return res.status(200).json(jsonApi.formatObject(type, 'carType'));

    });
  })
    .catch(err => {

      return res.status(400).json(jsonApi.formatError({
        status: 400,
        code: 'E_VALIDATION_ERROR',
        title: 'Validation error.',
        detail: 'The registration data contains errors.',
        meta: {
          errors: formatValidationErrors(err)
        }
      }));

    });
};

/**
 * Remove an existing car brand.
 */
export const removeCarType = (req, res) => {

  logger.info(`Delete car type with id: ${req.params.id}`);
  CarType.findByIdAndRemove(req.params.id, (err, type) => {

    if (err) {
      logger.error(err);

      return res.status(500).json(formatError({
        status: 500,
        code: 'E_REMOVE_CAR_TYPE_ERROR',
        title: 'Error removing a card type.',
        detail: 'This action could not complete due internal errors.'
      }));
    }

    if (!type) {
      return res.status(404).json({});
    }

    return res.status(204).json({});

  });

};
