'use strict';

import mongoose from 'mongoose';
import _ from 'lodash';
import getLogger from './../packages/logger';
import carModelSchema from './../validation/schemas/carModelSchema';
import formatValidationErrors from './../validation/formatter';
import * as jsonApi from './../packages/formatJsonApi';

let logger = getLogger();
let CarModel = mongoose.model('CarModel');

/**
 * Create a car model
 */
export const createCarModel = (req, res) => {

  logger.info('Creating car model');

  req.checkBody(carModelSchema);
  req.asyncValidationErrors(true).then(() => {

    const data = _.assign(req.body, { brand: req.carBrand.id });
    return CarModel.create(data, (err, model) => {
      if (err) {
        logger.error('Error creating a card model');
        logger.error(err);

        return res.status(500).json(jsonApi.formatError({
          status: 500,
          code: 'E_CREATE_CAR_MODEL_ERROR',
          title: 'Error creating the car model.',
          detail: 'This action could not complete due internal errors.'
        }));
      }

      return res.status(201).json(jsonApi.formatObject(model, 'carModel'));

    });
  })
  .catch(err => {
    logger.info(err);
    return res.status(400).json(jsonApi.formatError({
      status: 400,
      code: 'E_VALIDATION_ERROR',
      title: 'Validation error.',
      detail: 'The registration data contains errors.',
      meta: {
        errors: formatValidationErrors(err)
      }
    }));

  });

};


/**
 * List all car models.
 */
export const listCarModel = (req, res) => {

  logger.info(`List models of car brand ${req.carBrand.id}`);
  CarModel.paginate({brand: req.carBrand.id}, {page: req.query.page, limit: req.query.limit}, (err, result) => {

    if (err) {
      logger.error('Error retrieving the list of car model');
      logger.error(err);

      return res.status(500).json(jsonApi.formatError({
        status: 500,
        code: 'E_LIST_CAR_MODEL_ERROR',
        title: 'Error listing car models.',
        detail: 'This action could not complete due internal errors.'
      }));
    }

    return res.status(200).json(jsonApi.formatPagination(result, 'carModel'));

  });

};

/**
 * Update an existing car model.
 */
export const updateCarModel = (req, res) => {

  logger.info(`Update model with id: ${req.carModel.id}`);
  req.checkBody(carModelSchema);
  req.asyncValidationErrors(true).then(() => {

    CarModel.findByIdAndUpdate(req.carModel.id, req.body, {new: true}, (err, model) => {
      if (err) {
        logger.error('Error updating a card model');
        logger.error(err);

        return res.status(500).json(jsonApi.formatError({
          status: 500,
          code: 'E_CREATE_CAR_MODEL_ERROR',
          title: 'Error creating the car model.',
          detail: 'This action could not complete due internal errors.'
        }));
      }

      return res.status(200).json(jsonApi.formatObject(model, 'carModel'));

    });
  })
    .catch(err => {

      return res.status(400).json(jsonApi.formatError({
        status: 400,
        code: 'E_VALIDATION_ERROR',
        title: 'Validation error.',
        detail: 'The registration data contains errors.',
        meta: {
          errors: formatValidationErrors(err)
        }
      }));

    });
};

/**
 * Remove an existing car model.
 */
export const removeCarModel = (req, res) => {

  logger.info(`Delete model with id: ${req.carModel.id}`);
  CarModel.findOneAndRemove({'brand': req.carBrand.id, '_id': req.carModel.id}, (err, carModel) => {

    if (err) {
      logger.error(err);

      return res.status(500).json(formatError({
        status: 500,
        code: 'E_REMOVE_CAR_MODEL_ERROR',
        title: 'Error removing a card model.',
        detail: 'This action could not complete due internal errors.'
      }));
    }

    if (!carModel) {
      return res.status(404).json({});
    }

    return res.status(204).json({});

  });

};
