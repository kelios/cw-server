'use strict';

import mongoose from 'mongoose';
import _ from 'lodash';
import getLogger from './../packages/logger';
import createCarAction from './../actions/createCar';
import carSchema from './../validation/schemas/carSchema';
import formatValidationErrors from './../validation/formatter';
import * as jsonApi from './../packages/formatJsonApi';

let logger = getLogger();
let Car = mongoose.model('Car');

/**
 * Create a car
 */
export const createCar = (req, res) => {

  logger.info('Creating car');

  req.checkBody(carSchema);
  req.asyncValidationErrors(true).then(() => {

    const data = _.assign(req.body, {owner: req.user.id});

    createCarAction(data, (err, car) => {
      if (err) {
        logger.error('Error creating a car');
        logger.error(err);

        return res.status(500).json(jsonApi.formatError({
          status: 500,
          code: 'E_CREATE_CAR_ERROR',
          title: 'Error creating the car.',
          detail: 'This action could not complete due internal errors.'
        }));
      }

      return res.status(201).json(jsonApi.formatObject(car, 'car'));

    });
  })
  .catch(err => {

    return res.status(400).json(jsonApi.formatError({
      status: 400,
      code: 'E_VALIDATION_ERROR',
      title: 'Validation error.',
      detail: 'The registration data contains errors.',
      meta: {
        errors: formatValidationErrors(err)
      }
    }));

  });

};


/**
 * List all car.
 */
export const listCar = (req, res) => {
  logger.info('List user cars');

  const paginationOptions = {
    page: req.query.page,
    limit: req.query.limit,
    populate: Car.defaultPopulateOptions()
  };

  Car.paginate({owner: req.user.id}, paginationOptions, (err, result) => {

    if (err) {
      logger.error('Error retrieving the list of');
      logger.error(err);

      return res.status(500).json(jsonApi.formatError({
        status: 500,
        code: 'E_LIST_CAR_ERROR',
        title: 'Error listing cars.',
        detail: 'This action could not complete due internal errors.'
      }));
    }

    return res.status(200).json(jsonApi.formatPagination(result, 'car'));

  });

};


/**
 * Remove an existing car.
 */
export const removeCar = (req, res) => {

  Car.findOneAndRemove({_id: req.params.car, owner: req.user.id}, (err, car) => {

    if (err) {
      logger.error(err);

      return res.status(500).json(formatError({
        status: 500,
        code: 'E_REMOVE_CAR_ERROR',
        title: 'Error removing a car.',
        detail: 'This action could not complete due internal errors.'
      }));
    }

    if (!car) {
      return res.status(404).json({});
    }

    return res.status(204).json({});

  });

};
