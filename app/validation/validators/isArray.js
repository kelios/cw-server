'use strict';

/**
 * Ensure that the value is an array.
 */
export default value => Array.isArray(value);
