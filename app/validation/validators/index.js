'use strict';

import { default as isEmailAvailable } from './isEmailAvailable';
import { default as isArray } from './isArray';
import { default as isArrayLength } from './isArrayLength';

/**
 * Expose the list of custom validators.
 */
export default {
  isEmailAvailable,
  isArray,
  isArrayLength
};
