'use strict';

/**
 * Ensure that the array has the correct length.
 */
export default (value, options) => {
  if (!Array.isArray(value)) {
    return false;
  }

  if (options.min && value.length < options.min) {
    return false;
  }

  if (options.max && value.length > options.max) {
    return false;
  }

  return true;
}
