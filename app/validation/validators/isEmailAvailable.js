'use strict';

import mongoose from 'mongoose';
import getLogger from './../../packages/logger';

let User = mongoose.model('User');
let logger = getLogger();

/**
 * Ensure that the email isn't registered by other account.
 */
export default email => {

  // Async validator
  return new Promise((resolve, reject) => {

    if (!email) {
      return resolve();
    }

    // Query looking for an account with the email to validate.
    User.findOne({ email: email }, (err, result) => {

      // Reject in case of errors.
      if (err) {
        logger.error('Error in email validator');
        logger.error(err);

        return reject();
      }

      // Exists one user with the email.
      if (result) {
        return reject();
      }

      return resolve();

    });

  });

};
