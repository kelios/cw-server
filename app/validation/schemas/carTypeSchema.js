'use strict';

/**
 * Schema definition for Car Brand.
 */
export default {
  name: {
    notEmpty: true,
    errorMessage: 'The name is required.'
  }
};
