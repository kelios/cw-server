'use strict';

/**
 * Schema definition for a team.
 */
export default {
  plate: {
    notEmpty: true,
    errorMessage: 'The plate is required.'
  },
  zone: {
    notEmpty: true,
    errorMessage: 'The zone is required.'
  },
  members: {
    isArray: true,
    errorMessage: 'Required at least one member.',
    isArrayLength: {
      options: [{ min: 1 }]
    }
  }
};
