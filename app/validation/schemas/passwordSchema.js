'use strict';

export default {
  password: {
    notEmpty: true,
    errorMessage: 'The password is required.',
    isLength: {
      options: [{ min: 4, max: 50 }],
      errorMessage: 'The password should have at least 4 characters.'
    }
  }
};
