'use strict';

/**
 * Schema definition for zone.
 */
export default {
  name: {
    notEmpty: true,
    errorMessage: 'The name is required.'
  },
  address: {
    notEmpty: true,
    errorMessage: 'The address is required.'
  }
};
