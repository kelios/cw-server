'use strict';

/**
 * Schema definition for user registration.
 */
export default {
  name: {
    notEmpty: true,
    errorMessage: 'The name is required.'
  },
  password: {
    notEmpty: true,
    errorMessage: 'The password is required.',
    isLength: {
      options: [{ min: 4, max: 50 }],
      errorMessage: 'The password should have at least 4 characters.'
    }
  },
  email: {
    notEmpty: {
      errorMessage: 'The email is required.'
    },
    isEmail: {
      errorMessage: 'The email is not valid.'
    },
    isEmailAvailable: {
      errorMessage: 'The email is in use by another account.'
    }
  }
};
