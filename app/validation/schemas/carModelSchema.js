'use strict';

/**
 * Schema definition for Car Model.
 */
export default {
  name: {
    notEmpty: true,
    errorMessage: 'The name is required.'
  }
};
