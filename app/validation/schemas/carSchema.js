'use strict';

/**
 * Schema definition for Car Brand.
 */
export default {
  name: {
    notEmpty: true,
    errorMessage: 'The name is required.'
  },
  plate: {
    notEmpty: true,
    errorMessage: 'The plate is required.'
  },
  model: {
    notEmpty: true,
    errorMessage: 'The model is required.'
  }
};
