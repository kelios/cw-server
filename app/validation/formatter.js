'use strict';

import _ from 'lodash';

/**
 * Util function that map validation error into a JSON API errors field.
 * @param errors The list of errors from validation.
 */
export default errors => {
  // Don't dictate errors for undefined
  if (errors === undefined) {
    return {};
  }

  return _.mapValues(errors, (validation) => {
    return [validation.msg || ''];
  });
};
