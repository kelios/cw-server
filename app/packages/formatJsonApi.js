'use strict';

import _ from 'lodash';

/**
 * Normalize object representation.
 * @param obj
 * @param type
 * @param options
 */
export const formatObject = (obj, type, options={}) => {
  const attributes = _.isFunction(obj.toJSON) ? obj.toJSON() : obj;

  const jsonApi = options.jsonApi == undefined ? true : options.jsonApi;
  const dropFields = options.dropFields || [];
  const id = options.id || obj.id;
  const nested = options.nested || {};

  let objectJson = {
    type: type,
    id: id || obj.id,
    attributes: _.omit(attributes, _.concat(dropFields, ['__v', '_id']))
  };

  _.forEach(nested, (value, key) => {
    if (_.has(objectJson.attributes, key)) {
      objectJson.attributes[key] = formatObject(objectJson.attributes[key], value, { jsonApi: false });
    }
  });

  return jsonApi ? { data: objectJson } : objectJson;
};


/**
 * Normalize representation for a list of objects.
 * @param list
 * @param type
 * @param options
 */
export const formatList = (list, type, options={}) => {
  return _.map(list, obj => formatObject(obj, type, _.assign(options, { jsonApi: false })));
};

/**
 * Normalize paginated results.
 * @param results Result of mongoose-paginate.
 * @param type    Type of results.
 * @param options Format options
 */
export const formatPagination = (results, type, options={}) => {
  return {
    data: formatList(results.docs, type, options),
    meta: {
      page: results.page,
      limit: results.limit,
      total: results.total,
      pages: results.pages
    }
  };
};

/**
 * Normalize error response.
 * @param errors
 */
export const formatError = (errors) => {
  if (Array.isArray(errors)) {
    return {
      errors: errors
    };
  }

  return {
    errors: [errors]
  };
};
