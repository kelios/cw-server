'use strict';

/**
 * Return a middleware that check if the user has the rol specified.
 * If the user is missing the role, the request will be denied (403).
 *
 * @param role Role that the user should have.
 * @returns {function()} Middleware.
 */
export default role => {
  return function (req, res, next) {
    if (!req.user) {
      return res.status(401).json({
        status: 401,
        code: 'E_UNAUTHORIZED',
        title: 'You are not authorized.',
        detail: 'You are not authorized to perform this action; probably requires authentication.'
      });
    }

    if (role.toUpperCase() === 'ANY') {
      return next();
    }

    if (!req.user.roles || req.user.roles.indexOf(role.toUpperCase()) === -1) {
      return res.status(403).json({
        status: 403,
        code: 'E_DENIED',
        title: 'Action denied.',
        detail: 'You do not have the permissions.'
      });
    }

    return next();
  };
};
