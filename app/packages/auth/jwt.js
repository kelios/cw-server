'use strict';

import config from 'config';
import jwt from 'jwt-async';

let sessionConfig = config.session;

/**
 * Generate a JWT token to authorize an existing user use the API.
 * @see: https://jwt.io/
 * @param user The user to authorize.
 * @param done Callback to notify the token.
 */
export const generateJwtFromUser = (user, done) => {
  let token = new jwt({
    crypto: {
      algorithm: sessionConfig.algorithm,
      secret: sessionConfig.secret
    },
    claims: {
      iat: true,
      nbf: Math.floor(Date.now() / 1000) - 60,
      exp: Math.floor(Date.now() / 1000) + sessionConfig.expire,
      custom: user.username
    }
  });

  // This fields will be contained into the token.
  let payload = {
    id: user._id,
    email: user.email,
    roles: user.roles
  };

  token.setSecret(sessionConfig.secret);
  token.sign(payload, done);
};
