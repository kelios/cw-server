'use strict';

import mongoose from 'mongoose';
import getLogger from './../logger';

let User = mongoose.model('User');
let logger = getLogger();

/**
 * Middleware that detects a request authenticated and adds more info about the user.
 * @param req  Express request object.
 * @param res  Express response object.
 * @param next Call the next middleware.
 */
export default (req, res, next) => {
  if (!req.user) {
    return next();
  }

  User.findById(req.user.id, (err, user) => {
    if (err) {
      logger.error('Error looking for the user in the request token');
      logger.error(err);

      return next(err);
    }

    if (!user) {
      return res.status(401).json();
    }

    req.jwt = req.user;
    req.user = user;

    next();
  });
};
