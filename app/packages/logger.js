'use strict';

import config from 'config';
import winston from 'winston';
import winstonError from 'winston-error';

let logger = null;
let loggerConfig = config.logger;

/**
 * Logger instance of winston.
 */
export default () => {

  // Return the same logger configured before.
  if (logger) {
    return logger;
  }

  let transports = [];

  // Log to console.
  if (loggerConfig.console.enabled) {
    transports.push(new (winston.transports.Console)(loggerConfig.console));
  }

  // Log to file.
  if (loggerConfig.file.enabled) {
    transports.push(new (winston.transports.File)(loggerConfig.file));
  }

  // Create a winston instance with the transports configured.
  logger = new (winston.Logger)({
    transports: transports
  });

  // Enable to do:
  // logger.error(Error instance)
  // logger.error(new Error('fatal error'))
  winstonError(logger);

  return logger;

};
