'use strict';

import config from 'config';
import nodemailer from 'nodemailer';
import getLogger from './../logger';

let logger = getLogger();
let transporter = nodemailer.createTransport(config.smtp);

export default (message, done) => {

  transporter.sendMail(message, function(err, info) {

    if (err) {
      logger.error('Error sending email');
      logger.error(err);

      return done(err);
    }

    logger.debug('Email sent.');
    logger.debug(info);

    return done(null);

  });

}
