'use strict';

import config from 'config';
import swig from 'swig';

/**
 * Render a template to compose a message.
 * @param template Template
 * @param vars     Fulfill template with these values
 * @param title    Message subject
 * @param user     Recipient
 */
export default (template, vars, title, user) => {

  const templateMessage = config.templatesDir + template;
  const html = swig.renderFile(templateMessage, vars);

  return {
    from: 'Hippo CarWash <no-reply@hippo.cl>',
    to: `${user.name} ${user.last_name} <${user.email}>`,
    subject: title,
    html: html
  };

}
