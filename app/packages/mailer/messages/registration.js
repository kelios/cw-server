'use strict';

import composeMessage from './../compose';

/**
 * Registration message.
 * @param user User registered.
 * @param done Callback to notify.
 */
export default (user, done) => {

  // Fulfill template with these values.
  const vars = {
    user: user.toJSON(),
    token: user.confirmationToken,
  };

  const message = composeMessage('/registration.html', vars, 'Registration', user);

  done(null, message);
};
