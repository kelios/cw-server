'use strict';

import config from 'config';
import express from 'express';
import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import cors from 'cors';
import paginate from 'express-paginate';
import expressValidator from 'express-validator';
import passport from 'passport';
import expressJwt from 'express-jwt';
import customValidators from './validation/validators';
import configurePassport from './passport';
import convertJwtToUser from './packages/auth/middleware';
import router from './routes';

let app = express();
let appSpec = swaggerJSDoc({
  swaggerDefinition: {
    info: {
      title: 'Hippo API',
      version: '1.0'
    }
  },
  apis: [__dirname + '/definitions.yml', __dirname + '/routes/*.js']
});

app.use(helmet({
  frameguard: false,
  noSniff: false
}));

app.use(cors(config.cors));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(paginate.middleware(config.defaultLimit, config.maxLimit));
app.use(expressValidator({ customValidators: customValidators }));

configurePassport(passport);
app.use(passport.initialize());
app.use(expressJwt({ secret: config.session.secret}).unless({ path: config.firewall.anonymous }));
app.use(convertJwtToUser);

// Expose API
app.use('/api/docs', swaggerUi.serve, swaggerUi.setup(appSpec, false, { validatorUrl: null }));
app.use('/api/', router);

// Global handler.
app.use((err, req, res, next) => {
  if (err && err.status && err.status === 401) {
    return res.status(401).json({
      status: 401,
      code: 'E_UNAUTHORIZED',
      title: 'You are not authorized.',
      detail: 'You are not authorized to perform this action; probably requires authentication.'
    });
  }

  return next(err);
});

module.exports = app;
