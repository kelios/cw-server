'use strict';

import glob from 'glob';
import path from 'path';
import config from 'config';
import mongoose from 'mongoose';

let models = glob.sync(__dirname + '/models/**/*.js');

/**
 * Boot all services that the API relies on.
 * @param logger Logger instance.
 * @param done   Callback to notify.
 */
export default (logger, done) => {

  logger.debug('Connecting to MongoDB');

  mongoose.Promise = require('bluebird');
  mongoose.connect(config.mongodb.dsn, config.mongodb.options, err => {

    if (err) {
      return done(err);
    }

    // Register all models to use:
    // let model = mongoose.model(<Model registered>)
    models.forEach(function(modelPath){
      let model = path.resolve(modelPath);
      require(model);
    });

    logger.info('Connected to MongoDB');

    return done(err);
  });

};
