'use strict';

import config from 'config';
import bootstrap from './bootstrap';
import getLogger from './packages/logger';

let logger = getLogger();

bootstrap(logger, err => {

  if (err) {
    return logger.error(err);
  }

  // Start listening on configured port
  var app = require('./main');

  let server = app.listen(config.port, () => {
    let addr = server.address();
    let bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;

    logger.info('Listening on ' + bind);
    logger.info("Started application on mode " + app.get("env"));
  });

});
