
'use strict';

import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const carBrandSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  image: {
    type: String,
    required: false
  }
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  }
});

carBrandSchema.plugin(mongoosePaginate);


export default mongoose.model('CarBrand', carBrandSchema, 'CarBrand');
