
'use strict';

import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const ObjectId = mongoose.Schema.ObjectId;

const carSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  plate: {
    type: String,
    required: true,
    trim: true,
  },
  model: {
    type: ObjectId,
    ref: 'CarModel',
    required: true
  },
  owner: {
    type: ObjectId,
    ref: 'User',
    required: true
  },
  carType: {
    type: ObjectId,
    ref: 'CarType',
    required: true
  },

}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  }
});

const defaultPopulateOptions = () => [
  {
    path: 'model',
    select: 'id name',
    options: {
      sort: {name: -1},
      lean: true,
      leanWithId: true
    }
  },
  {
    path: 'owner',
    select: 'id name',
    options: {
      sort: {name: -1},
      lean: true,
      leanWithId: true
    }
  },
  {
    path: 'carType',
    select: 'id name',
    options: {
      sort: {name: -1},
      lean: true,
      leanWithId: true
    }
  }
];

carSchema.method('defaultPopulateOptions', defaultPopulateOptions);

carSchema.static('defaultPopulateOptions', defaultPopulateOptions);

carSchema.plugin(mongoosePaginate);

export default mongoose.model('Car', carSchema, 'Car');
