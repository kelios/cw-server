
'use strict';

import mongoose from 'mongoose';
import mongoosePaginatePlugin from 'mongoose-paginate';

let TeamSchema = new mongoose.Schema({
  plate: {
    type: String,
    required: true,
    trim: true,
  },
  zone: {
    type: mongoose.Schema.ObjectId,
    ref: 'Zone',
    required: true
  },
  members: [{
    type: String,
    required: true
  }]
}, {
  // Activate timestamp native plugin for this schema.
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  }
});

// Allow queries with pagination.
TeamSchema.plugin(mongoosePaginatePlugin);

export default mongoose.model('Team', TeamSchema, 'Team');
