
'use strict';

import mongoose from 'mongoose';
import mongooseBcryptPlugin from 'mongoose-bcrypt';
import mongoosePaginatePlugin from 'mongoose-paginate';

let validateLocalStrategyPassword = function(password) {
  return (this.provider !== 'local' || !password || password.length >= 4);
};

let UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },

  email: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    unique: 'Email in use',
    match: [/.+\@.+\..+/, 'Please fill a valid email address']
  },

  password: {
    type: String,
    required: true,
    default: '',
    bcrypt: true,
    validate: [validateLocalStrategyPassword, 'Password should be longer']
  },

  dni: {
    type: String,
    required: false,
    trim: true,
    lowercase: true,
    unique: 'DNI in use',
    index: true,
    sparse: true
  },

  provider: {
    type: String,
    required: 'Provider is required',
    default: 'local',
    lowercase: true
  },

  facebook: {
    type: String,
    required: false
  },

  twitter: {
    type: String,
    required: false
  },

  confirmedAt: {
    type: Date,
    required: false,
    default: null
  },

  confirmationToken: {
    type: String,
    required: false,
    default: null
  },

  roles: [String]
}, {
  // Activate timestamp native plugin for this schema.
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  }
});

/**
 * Virtual property to check if the account needs confirmation or not.
 * @see: http://mongoosejs.com/docs/api.html#virtualtype_VirtualType-get
 */
UserSchema.virtual('pendingConfirmation').get(function() {
  return this.provider === 'local' && this.confirmedAt === null;
});

// Plugin to encrypt password field.
UserSchema.plugin(mongooseBcryptPlugin);

// Allow queries with pagination.
UserSchema.plugin(mongoosePaginatePlugin);

export default mongoose.model('User', UserSchema, 'User');
