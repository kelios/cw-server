
'use strict';

import mongoose from 'mongoose';
import mongoosePaginatePlugin from 'mongoose-paginate';

let ZoneSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },

  address: {
    type: String,
    required: true,
    trim: true
  }
}, {
  // Activate timestamp native plugin for this schema.
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  }
});

// Allow queries with pagination.
ZoneSchema.plugin(mongoosePaginatePlugin);

export default mongoose.model('Zone', ZoneSchema, 'Zone');
