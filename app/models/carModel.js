
'use strict';

import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const ObjectId = mongoose.Schema.ObjectId;

const carModelSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  brand: {
    type: ObjectId,
    ref: 'CarBrand',
    required: true
  }
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  }
});

carModelSchema.plugin(mongoosePaginate);

export default mongoose.model('CarModel', carModelSchema, 'CarModel');
