
'use strict';

import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate'

const ObjectId = mongoose.Schema.ObjectId;

const carTypeSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  models: [
    {
      type: ObjectId,
      ref: 'CarModel'
    }
  ],
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  }
});

const defaultPopulateOptions = () => ({
  path: 'models',
    select: 'id name',
    options: {
    sort: {name: -1},
    lean: true,
      leanWithId: true
  }
});

carTypeSchema.method('defaultPopulateOptions', defaultPopulateOptions);

carTypeSchema.static('defaultPopulateOptions', defaultPopulateOptions);

carTypeSchema.plugin(mongoosePaginate);


export default mongoose.model('CarType', carTypeSchema, 'CarType');
