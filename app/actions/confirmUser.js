'use strict';

/**
 * Confirm the user.
 * @param user User to confirm.
 * @param done Callback to notify.
 */
export default (user, done) => {

  user.confirmationToken = null;
  user.confirmedAt = new Date();

  user.save(err => done(err));

}
