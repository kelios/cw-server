'use strict';

import getLogger from './../packages/logger';
import { generateJwtFromUser } from './../packages/auth/jwt';
import { formatObject } from './../packages/formatJsonApi';

let logger = getLogger();

/**
 * Generate a session token (JWT) for the user.
 * @param user User to authenticate.
 * @param done Callback to notify results.
 */
export default (user, done) => {

  generateJwtFromUser(user, (err, token) => {
    if (err) {
      logger.error('Error creating the session token');
      logger.error(err);

      return done(err, null);
    }

    done(err, {
      token: token,
      user: formatObject(user, 'user')
    });

  });

};
