'use strict';

import mongoose from 'mongoose';
import shortid from 'shortid';
import waterfall from 'async/waterfall';
import mailer from './../packages/mailer/sender';
import registrationMessage from './../packages/mailer/messages/registration';
import getLogger from './../packages/logger';

let User = mongoose.model('User');
let logger = getLogger();

/**
 * Create a new user.
 * @param data Registration data.
 * @param done Callback to notify the registration.
 */
export default (data, done) => {

  waterfall([

    // Create the user.
    next => User.create(data, (err, user) => {

      if (err) {
        logger.error('Error creating the user');
        logger.error(err);
      }

      next(err, user)

    }),

    // Set confirmation token.
    (user, next) => {

      if (user.provider !== 'local') {
        return next(null, user);
      }

      user.confirmationToken = shortid.generate();
      user.save((err, user) => next(err, user));

    },


    // Send registration email
    (user, next) => registrationMessage(user, (err, message) => {

      if (err) {
        logger.error(err);

        return next(err);
      }

      mailer(message, err => next(err, user));

    })

  ], (err, user) => done(err, user));

};
