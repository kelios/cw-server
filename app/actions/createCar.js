'use strict';

import mongoose from 'mongoose';
import _ from 'lodash';
import waterfall from 'async/waterfall';
import getLogger from './../packages/logger';

let Car = mongoose.model('Car');
let CarType = mongoose.model('CarType');
let logger = getLogger();

/**
 * Create a new car.
 * @param data Registration data.
 * @param done Callback to notify the registration.
 */
export default (data, done) => {

  waterfall([

    // Find the type of the car.
    next => CarType.findOne({models: {'$in': [data.model]}}, (err, carType) => {

      if (err) {
        logger.error(`Error finding one type of car with model ${data.model}`);
        logger.error(err);
      }

      next(err, carType);
    }),

    // Set the Type to the Car data.
    (carType, next) => Car.create(_.assign(data, {carType: carType.id}), (err, car) => {
      Car.populate(car, car.defaultPopulateOptions(), next);
    })
  ], (err, car) => done(err, car));

};
