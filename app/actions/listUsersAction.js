'use strict';

import mongoose from 'mongoose';

let User = mongoose.model('User');

export default (filters, done) => {
  let options = {
    page: filters.page,
    limit: filters.limit,
    sort: filters.sort || null,
    lean: true
  };

  let criteria = {};

  if (filters.q && filters.q.length !== 0) {
    const matchRegEx = new RegExp(filters.q, 'i');
    criteria['$or'] = [
      { name: matchRegEx },  // full-text match
      { email: matchRegEx }, // full-text match
      { dni: filters.q },    // exact match
    ];
  }

  User.paginate(criteria, options, (err, results) => done(err, results));

};
