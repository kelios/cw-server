'use strict';

import mongoose from 'mongoose';
import getLogger from './../packages/logger';

let logger = getLogger();
let Zone = mongoose.model('Zone');

export default (data, done) => {

  Zone.create(data, (err, zone) => {
    if (err) {
      logger.error('Error creating a zone');
      logger.error(err);

      return done(err, null);
    }

    done(err, zone);

  });

};
