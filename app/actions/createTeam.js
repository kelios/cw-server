'use strict';

import mongoose from 'mongoose';

let Team = mongoose.model('Team');

/**
 * Create a team.
 * @param data Registration data.
 * @param done Callback to notify the registration.
 */
export default (data, done) => {

  Team.create(data, (err, team) => {

    if (err) {
      return done(err, null);
    }

    Team.findById(team.id)
      .populate('zone')
      .exec((err, t) => done(err, t));

  });

};
