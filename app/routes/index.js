'use strict';

import Router from 'express';
import { default as user } from './user';
import { default as session } from './session';
import { default as zone } from './zone';
import carBrand from './carBrand';
import carType from './carType';
import team from './team';

let router = Router();

router.use('/users', user);
router.use('/sessions', session);
router.use('/zones', zone);
router.use('/car-brands', carBrand);
router.use('/car-types', carType);
router.use('/teams', team);

export default router;
