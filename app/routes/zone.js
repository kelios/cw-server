'use strict';

import Router from 'express';
import config from 'config';
import expressJwt from 'express-jwt';
import { createZone, listZones, removeZone } from './../controllers/zone';
import hasRole from './../packages/auth/hasRole';

let router = Router();
const checkJwt = expressJwt({ secret: config.session.secret });
const checkForAdmin = hasRole('ROLE_ADMIN');

/**
 * @swagger
 * /api/zones:
 *  post:
 *    description: "Creates a zone (a.k.a comuna)."
 *    operationId: createZone
 *    produces:
 *      - application/json
 *    tags:
 *      - Zone
 *    parameters:
 *      - name: name
 *        in: body
 *        description: "Full name"
 *        required: true
 *        schema:
 *          type: string
 *      - name: address
 *        in: body
 *        description: "Address"
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      201:
 *        description: "Zone created."
 *        schema:
 *          $ref: '#/definitions/ZoneResponse'
 *      400:
 *        description: "Validation problem"
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.post('/', checkJwt, checkForAdmin, createZone);

/**
 * @swagger
 * /api/zones:
 *  get:
 *    description: "Get list of zones"
 *    operationId: listZones
 *    produces:
 *      - application/json
 *    tags:
 *      - Zone
 *    responses:
 *      200:
 *        description: "Zones."
 *        schema:
 *          $ref: '#/definitions/ZoneListResponse'
 *      404:
 *        description: "Not Found."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.get('/', listZones);

/**
 * @swagger
 * /api/zones/{zone}:
 *  delete:
 *    description: "Remove an existing zone"
 *    operationId: removeZone
 *    produces:
 *      - application/json
 *    tags:
 *      - Zone
 *    parameters:
 *      - name: zone
 *        in: query
 *        description: "Zone ID"
 *        required: true
 *        type: string
 *    responses:
 *      204:
 *        description: "Zone removed."
 *      404:
 *        description: "Not Found."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.delete('/:zone', checkForAdmin, removeZone);

export default router;
