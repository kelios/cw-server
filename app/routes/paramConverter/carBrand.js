'use strict';

import mongoose from 'mongoose';
import getLogger from './../../packages/logger';
import { formatError } from './../../packages/formatJsonApi';

let logger = getLogger();
let CarBrand = mongoose.model('CarBrand');

/**
 * Route middleware that detects parameter :user in the url and parse into the User object.
 *
 * @param req  Express request object.
 * @param res  Express response object.
 * @param next Continue with the next middleware.
 * @param brand Placeholder in the url representing a car id.
 */
export default (req, res, next, brand) => {
  if (!brand.match(/^[0-9a-fA-F]{24}$/)) {
    logger.error('Error in paramConverter car brand.');

    const err = 'Incorrect format or id';
    logger.error(err);

    return next(err);
  }

  CarBrand.findById(brand, (err, carBrand) => {

    if (err) {
      logger.error('Error in paramConverter carBrand');
      logger.error(err);

      return next(err);
    }

    if (!carBrand) {
      return res.status(404).json(formatError({
        status: 404,
        code: 'E_NOT_FOUND_ERROR',
        title: 'Car brand not found.',
        detail: 'The car brand was not found.'
      }));
    }

    req.carBrand = carBrand;

    return next();

  });

};
