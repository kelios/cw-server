'use strict';

import mongoose from 'mongoose';
import getLogger from './../../packages/logger';
import { formatError } from './../../packages/formatJsonApi';

let logger = getLogger();
let User = mongoose.model('User');

/**
 * Route middleware that detects parameter :user in the url and parse into the User object.
 *
 * @param req  Express request object.
 * @param res  Express response object.
 * @param next Continue with the next middleware.
 * @param user Placeholder in the url representing a technology id.
 */
export default (req, res, next, user) => {

  // Do not make an extra query for the user authenticated
  if (req.user && req.user.hasOwnProperty('dni') && req.user.dni.toString () === user.toString() || user == 'me') {
    req.ruser = req.user;

    return next();
  }

  let criteria = [ { dni: user } ];
  if (user.match(/^[0-9a-fA-F]{24}$/)) {
    criteria.push({ _id: user });
  }

  User.findOne({ $or: criteria }, (err, user) => {

    if (err) {
      logger.error('Error in paramConverter user');
      logger.error(err);

      return next(err);
    }

    if (!user) {
      return res.status(404).json(formatError({
        status: 404,
        code: 'E_NOT_FOUND_ERROR',
        title: 'User not found.',
        detail: 'The user was not found.'
      }));
    }

    req.ruser = user;

    return next();

  });

};
