'use strict';

import mongoose from 'mongoose';
import getLogger from './../../packages/logger';
import { formatError } from './../../packages/formatJsonApi';

let logger = getLogger();
let CarModel = mongoose.model('CarModel');

/**
 * Route middleware that detects parameter :user in the url and parse into the User object.
 *
 * @param req  Express request object.
 * @param res  Express response object.
 * @param next Continue with the next middleware.
 * @param model Placeholder in the url representing a car model id.
 */
export default (req, res, next, model) => {

  if (!model.match(/^[0-9a-fA-F]{24}$/)) {
    logger.error('Error in paramConverter car model.');

    const err = 'Incorrect format or id';
    logger.error(err);

    return next(err);
  }

  CarModel.findById(model).populate('brand').exec((err, model) => {

    if (err) {
      logger.error('Error in paramConverter car model');
      logger.error(err);

      return next(err);
    }

    // todo: Can a model be reassigned to another brand?
    if (!model || model.brand.id != req.carBrand.id) {
      return res.status(404).json(formatError({
        status: 404,
        code: 'E_NOT_FOUND_ERROR',
        title: 'Car model not found.',
        detail: 'The car model was not found.'
      }));
    }

    req.carModel = model;

    return next();

  });

};
