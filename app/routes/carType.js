'use strict';

import Router from 'express';
import * as controller from './../controllers/carType';

let router = Router();

/**
 * @swagger
 * /api/car-types:
 *  post:
 *    description: "Create a new car type."
 *    operationId: createCarType
 *    produces:
 *      - application/json
 *    tags:
 *      - Car type
 *    parameters:
 *      - name: name
 *        in: body
 *        description: "Name of the type."
 *        required: true
 *        schema:
 *          type: string
 *      - name: models
 *        in: body
 *        description: "List of models database IDs."
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      201:
 *        description: "Car type was created."
 *        schema:
 *          $ref: '#/definitions/CarTypeResponse'
 *      400:
 *        description: "Validation problem"
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.post('/', controller.createCarType);

/**
 * @swagger
 * /api/car-types:
 *  get:
 *    description: "List car types"
 *    operationId: listCarType
 *    produces:
 *      - application/json
 *    tags:
 *      - Car type
 *    parameters:
 *      - name: page
 *        in: query
 *        description: "Page to list."
 *        required: false
 *        type: number
 *        default: 1
 *      - name: limit
 *        in: query
 *        description: "Size of each page listed."
 *        type: number
 *        required: false
 *        default: 25
 *    responses:
 *      200:
 *        description: "List of car types."
 *        schema:
 *          $ref: '#/definitions/CarTypeListResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.get('/', controller.listCarType);

/**
 * @swagger
 * /api/car-types/{id}:
 *  delete:
 *    description: "Remove a car type."
 *    operationId: removeCarType
 *    produces:
 *      - application/json
 *    tags:
 *      - Car type
 *    parameters:
 *      - name: id
 *        in: query
 *        description: "Car type database ID"
 *        required: true
 *        type: string
 *    responses:
 *      204:
 *        description: "The car type was removed."
 *      404:
 *        description: "Car type not found"
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.delete('/:id', controller.removeCarType);

/**
 * @swagger
 * /api/car-types/{id}:
 *  put:
 *    description: "Update a car type."
 *    operationId: updateCarType
 *    produces:
 *      - application/json
 *    tags:
 *      - Car type
 *    parameters:
 *      - name: id
 *        in: query
 *        description: "Car type database ID"
 *        required: true
 *        schema:
 *          type: string
 *      - name: name
 *        in: body
 *        description: "Name of the type."
 *        required: true
 *        schema:
 *          type: string
 *      - name: models
 *        in: body
 *        description: "List of models database IDs."
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: "Car type was updated."
 *        schema:
 *          $ref: '#/definitions/CarTypeResponse'
 *      400:
 *        description: "Validation problem"
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.put('/:id', controller.updateCarType);

export default router;
