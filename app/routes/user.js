'use strict';

import Router from 'express';
import config from 'config';
import expressJwt from 'express-jwt';
import userConverter from './paramConverter/user';
import hasRole from './../packages/auth/hasRole';
import { createUser, listUsers, confirmUser, getUser, changePassword } from './../controllers/user';
import * as carController from './../controllers/car';

let router = Router();
const checkJwt = expressJwt({ secret: config.session.secret });
const checkForAdmin = hasRole('ROLE_ADMIN');

router.param('user', userConverter);

/**
 * @swagger
 * /api/users:
 *  post:
 *    description: "Creates an user account."
 *    operationId: createUser
 *    produces:
 *      - application/json
 *    tags:
 *      - User
 *    parameters:
 *      - name: name
 *        in: query
 *        description: "Full name"
 *        required: true
 *        type: string
 *      - name: email
 *        in: body
 *        description: "Email"
 *        type: string
 *        required: true
 *      - name: password
 *        in: body
 *        description: "Registration password"
 *        required: true
 *        type: string
 *      - name: dni
 *        in: body
 *        description: "Document ID or DNI"
 *        required: false
 *        type: string
 *    responses:
 *      201:
 *        description: "User created. Session token."
 *        schema:
 *          $ref: '#/definitions/SessionResponse'
 *      400:
 *        description: "Validation problem"
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.post('/', createUser);

/**
 * @swagger
 * /api/users:
 *  get:
 *    description: "List users."
 *    operationId: listUsers
 *    produces:
 *      - application/json
 *    tags:
 *      - User
 *    parameters:
 *      - name: page
 *        in: query
 *        description: "Page to list."
 *        required: false
 *        type: number
 *        default: 1
 *      - name: limit
 *        in: query
 *        description: "Size of each page listed."
 *        type: number
 *        required: false
 *        default: 25
 *      - name: sort
 *        in: query
 *        description: "Sort the list, accept any field in the user. Specify a minus sign to sort in descending order."
 *        type: string
 *        required: false
 *        enum: ["name", "-name", "createdAt", "-createdAt"]
 *      - name: q
 *        in: query
 *        description: "Full-text match in fields name and email, exact match against field dni."
 *        type: string
 *        required: false
 *    responses:
 *      200:
 *        description: "List of users."
 *        schema:
 *          $ref: '#/definitions/UserListResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.get('/', checkJwt, checkForAdmin, listUsers);

/**
 * @swagger
 * /api/users/{user}/confirmations/{token}:
 *  delete:
 *    description: "Confirm user."
 *    operationId: confirmUser
 *    produces:
 *      - application/json
 *    tags:
 *      - User
 *    parameters:
 *      - name: user
 *        in: query
 *        description: "DNI or ID"
 *        required: true
 *        type: string
 *      - name: token
 *        in: query
 *        description: "Confirmation token"
 *        type: string
 *        required: true
 *    responses:
 *      204:
 *        description: "The user was confirmed."
 *      404:
 *        description: "User not found"
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.delete('/:user/confirmations/:token', confirmUser);

/**
 * @swagger
 * /api/users/{user}:
 *  get:
 *    description: "Get details of user."
 *    operationId: getUser
 *    produces:
 *      - application/json
 *    tags:
 *      - User
 *    parameters:
 *      - name: user
 *        in: query
 *        description: "DNI or ID"
 *        required: true
 *        type: string
 *    responses:
 *      204:
 *        description: "The user."
 *        schema:
 *          $ref: '#/definitions/UserResponse'
 *      404:
 *        description: "User not found"
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.get('/:user', getUser);

/**
 * @swagger
 * /api/users/me/passwords:
 *  post:
 *    description: "Change password."
 *    operationId: changePassword
 *    produces:
 *      - application/json
 *    tags:
 *      - User
 *      - Profile
 *    parameters:
 *      - name: user
 *        in: query
 *        description: "DNI or ID"
 *        required: true
 *        type: string
 *      - name: password
 *        in: body
 *        description: "New password"
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      202:
 *        description: "Password changed."
 *      404:
 *        description: "User not found"
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.post('/:user/passwords', changePassword);

/**
 * @swagger
 * /api/users/{user}/cars:
 *  post:
 *    description: "Create a new car for an user."
 *    operationId: createCar
 *    produces:
 *      - application/json
 *    tags:
 *      - User
 *      - Car
 *    parameters:
 *      - name: user
 *        in: query
 *        description: "User database ID"
 *        required: true
 *        schema:
 *          type: string
 *      - name: name
 *        in: body
 *        description: "String to identify the car."
 *        required: true
 *        schema:
 *          type: string
 *      - name: plate
 *        in: body
 *        description: "Car's plate"
 *        required: true
 *        schema:
 *          type: string
 *      - name: model
 *        in: body
 *        description: "Car's model"
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      201:
 *        description: "Car was created."
 *        schema:
 *          $ref: '#/definitions/CarResponse'
 *      400:
 *        description: "Validation problem"
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      404:
 *        description: "User not found"
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.post('/:user/cars', carController.createCar);

/**
 * @swagger
 * /api/users/{user}/cars:
 *  get:
 *    description: "List user's cars"
 *    operationId: listCar
 *    produces:
 *      - application/json
 *    tags:
 *      - User
 *      - Car
 *    parameters:
 *      - name: user
 *        in: query
 *        description: "User database ID"
 *        required: true
 *        schema:
 *          type: string
 *      - name: page
 *        in: query
 *        description: "Page to list."
 *        required: false
 *        type: number
 *        default: 1
 *      - name: limit
 *        in: query
 *        description: "Size of each page listed."
 *        type: number
 *        required: false
 *        default: 25
 *    responses:
 *      200:
 *        description: "List of user's cars."
 *        schema:
 *          $ref: '#/definitions/UserListResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.get('/:user/cars', carController.listCar);

/**
 * @swagger
 * /api/users/{user}/cars/{car}:
 *  delete:
 *    description: "Remove car."
 *    operationId: removeCar
 *    produces:
 *      - application/json
 *    tags:
 *      - User
 *      - Car
 *    parameters:
 *      - name: user
 *        in: query
 *        description: "User database ID"
 *        required: true
 *        type: string
 *      - name: car
 *        in: query
 *        description: "Car database ID"
 *        required: true
 *        type: string
 *    responses:
 *      204:
 *        description: "The car was removed."
 *      404:
 *        description: "Car not found"
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.delete('/:users/cars/:car', carController.removeCar);

export default router;
