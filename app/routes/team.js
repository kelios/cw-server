'use strict';

import Router from 'express';
import config from 'config';
import expressJwt from 'express-jwt';
import hasRole from './../packages/auth/hasRole';
import { createTeam, listTeam, removeTeam } from './../controllers/team';

let router = Router();
const checkJwt = expressJwt({ secret: config.session.secret });
const checkForAdmin = hasRole('ROLE_ADMIN');

/**
 * @swagger
 * /api/teams:
 *  post:
 *    description: "Activate a team by creating a new one."
 *    operationId: createTeam
 *    produces:
 *      - application/json
 *    tags:
 *      - Team
 *    parameters:
 *      - name: plate
 *        in: body
 *        description: "Car number or plate"
 *        required: true
 *        schema:
 *          type: string
 *      - name: zone
 *        in: body
 *        description: "Zone Id"
 *        required: true
 *        schema:
 *          type: ObjectId
 *      - name: members
 *        in: body
 *        description: "List of members"
 *        schema:
 *          type: array
 *          items:
 *            type: string
 *    responses:
 *      201:
 *        description: "Team created."
 *        schema:
 *          $ref: '#/definitions/TeamResponse'
 *      400:
 *        description: "Validation problem"
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.post('/', createTeam);

/**
 * @swagger
 * /api/teams:
 *  get:
 *    description: "Get the list of existing teams."
 *    operationId: listTeams
 *    produces:
 *      - application/json
 *    tags:
 *      - Team
 *    responses:
 *      200:
 *        description: "Teams."
 *        schema:
 *          $ref: '#/definitions/TeamListResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.get('/', checkJwt, checkForAdmin, listTeam);

/**
 * @swagger
 * /api/teams/{team}:
 *  delete:
 *    description: "Remove an existing team"
 *    operationId: removeTeam
 *    produces:
 *      - application/json
 *    tags:
 *      - Team
 *    parameters:
 *      - name: team
 *        in: query
 *        description: "Team ID"
 *        required: true
 *        type: string
 *    responses:
 *      204:
 *        description: "Team removed."
 *      404:
 *        description: "Not Found."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.delete('/:team', checkJwt, checkForAdmin, removeTeam);

export default router;
