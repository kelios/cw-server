'use strict';

import Router from 'express';
import { createSession } from './../controllers/session';

let router = Router();

/**
 * @swagger
 * /api/sessions:
 *  post:
 *    description: "Creates an authorization token to make call to the rest of the API endpoints."
 *    operationId: createSession
 *    produces:
 *      - application/json
 *    tags:
 *      - Authentication
 *    parameters:
 *      - name: provider
 *        in: query
 *        description: "Authentication method to use. Supported methods are local, facebook and twitter"
 *        required: true
 *        type: string
 *        enum: ["local", "facebook", "twitter"]
 *        default: "local"
 *      - name: login
 *        in: body
 *        description: "Email or DNI. (Applicable only with provider = local)"
 *        type: string
 *        required: false
 *      - name: password
 *        in: body
 *        description: "Registration password (Applicable only with provider = local)"
 *        required: false
 *        type: string
 *      - name: access_token
 *        in: query
 *        description: "Facebook access token after authorize the app (Applicable only with provider = facebook)"
 *        required: false
 *        type: string
 *      - name: oauth_token
 *        in: query
 *        description: "(Applicable only with provider = twitter)"
 *        type: string
 *        required: false
 *      - name: oauth_token_secret
 *        in: query
 *        description: "(Applicable only with provider = twitter)"
 *        type: string
 *        required: false
 *      - name: user_id
 *        in: query
 *        description: "User authenticated in Twitter (Applicable only with provider = twitter)."
 *        type: string
 *        required: false
 *    responses:
 *      201:
 *        description: "Session created."
 *        schema:
 *          $ref: '#/definitions/SessionResponse'
 *      401:
 *        description: "Not authorized to login. Wrong credentials."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.post('/', createSession);

export default router;
