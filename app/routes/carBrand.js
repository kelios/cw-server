'use strict';

import Router from 'express';
import * as brandController from './../controllers/carBrand';
import * as modelController from './../controllers/carModel';
import brandConverter from './paramConverter/carBrand';
import modelConverter from './paramConverter/carModel';

let router = Router();

router.param('brand', brandConverter);
router.param('model', modelConverter);

/**
 * @swagger
 * /api/car-brands:
 *  post:
 *    description: "Create a new car brand."
 *    operationId: createCarBrand
 *    produces:
 *      - application/json
 *    tags:
 *      - Car brand
 *    parameters:
 *      - name: name
 *        in: body
 *        description: "Name of the brand."
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      201:
 *        description: "Car brand was created."
 *        schema:
 *          $ref: '#/definitions/CarBrandResponse'
 *      400:
 *        description: "Validation problem"
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.post('/', brandController.createCarBrand);

/**
 * @swagger
 * /api/car-brands:
 *  get:
 *    description: "List car brands"
 *    operationId: listCarBrand
 *    produces:
 *      - application/json
 *    tags:
 *      - Car brand
 *    parameters:
 *      - name: page
 *        in: query
 *        description: "Page to list."
 *        required: false
 *        type: number
 *        default: 1
 *      - name: limit
 *        in: query
 *        description: "Size of each page listed."
 *        type: number
 *        required: false
 *        default: 25
 *    responses:
 *      200:
 *        description: "List of car brands."
 *        schema:
 *          $ref: '#/definitions/CarBrandListResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.get('/', brandController.listCarBrand);

/**
 * @swagger
 * /api/car-brand/{brand}:
 *  delete:
 *    description: "Remove a car brand."
 *    operationId: removeCarBrand
 *    produces:
 *      - application/json
 *    tags:
 *      - Car brand
 *    parameters:
 *      - name: brand
 *        in: query
 *        description: "Car brand database ID"
 *        required: true
 *        type: string
 *    responses:
 *      204:
 *        description: "The car brand was removed."
 *      404:
 *        description: "Car brand not found"
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.delete('/:brand', brandController.removeCarBrand);

/**
 * @swagger
 * /api/car-brands/{brand}:
 *  put:
 *    description: "Update a car brand."
 *    operationId: updateCarBrand
 *    produces:
 *      - application/json
 *    tags:
 *      - Car brand
 *    parameters:
 *      - name: brand
 *        in: query
 *        description: "Car brand database ID"
 *        required: true
 *        schema:
 *          type: string
 *      - name: name
 *        in: body
 *        description: "Name of the brand."
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: "Car brand was updated."
 *        schema:
 *          $ref: '#/definitions/CarBrandResponse'
 *      400:
 *        description: "Validation problem"
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.put('/:brand', brandController.updateCarBrand);


/**
 * @swagger
 * /api/car-brands/{brand}/models:
 *  post:
 *    description: "Create a new model in a car brand."
 *    operationId: createCarModel
 *    produces:
 *      - application/json
 *    tags:
 *      - Car model
 *    parameters:
 *      - name: brand
 *        in: query
 *        description: "Car brand database ID."
 *        required: true
 *        schema:
 *          type: string
 *      - name: name
 *        in: body
 *        description: "Name of the model."
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      201:
 *        description: "Car model was created."
 *        schema:
 *          $ref: '#/definitions/CarModelResponse'
 *      400:
 *        description: "Validation problem"
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.post('/:brand/models', modelController.createCarModel);

/**
 * @swagger
 * /api/car-brands/{brand}/models:
 *  get:
 *    description: "List car models"
 *    operationId: listCarModel
 *    produces:
 *      - application/json
 *    tags:
 *      - Car model
 *    parameters:
 *      - name: brand
 *        in: query
 *        description: "Car brand database ID."
 *        required: true
 *        type: String
 *      - name: page
 *        in: query
 *        description: "Page to list."
 *        required: false
 *        type: number
 *        default: 1
 *      - name: limit
 *        in: query
 *        description: "Size of each page listed."
 *        type: number
 *        required: false
 *        default: 25
 *    responses:
 *      200:
 *        description: "List of car brands."
 *        schema:
 *          $ref: '#/definitions/CarModelListResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.get('/:brand/models', modelController.listCarModel);

/**
 * @swagger
 * /api/car-brands/{brand}/models/{model}:
 *  put:
 *    description: "Update a car model."
 *    operationId: updateCarModel
 *    produces:
 *      - application/json
 *    tags:
 *      - Car model
 *    parameters:
 *      - name: brand
 *        in: query
 *        description: "Car brand database ID"
 *        required: true
 *        schema:
 *          type: string
 *      - name: model
 *        in: query
 *        description: "Car model database ID"
 *        required: true
 *        schema:
 *          type: string
 *      - name: name
 *        in: body
 *        description: "Name of the model."
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: "Car model was updated."
 *        schema:
 *          $ref: '#/definitions/CarModelResponse'
 *      400:
 *        description: "Validation problem"
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.put('/:brand/models/:model', modelController.updateCarModel);

/**
 * @swagger
 * /api/car-brand/{brand}/models/{model}:
 *  delete:
 *    description: "Remove a car model."
 *    operationId: removeCarModel
 *    produces:
 *      - application/json
 *    tags:
 *      - Car model
 *    parameters:
 *      - name: brand
 *        in: query
 *        description: "Car brand database ID"
 *        required: true
 *        type: string
 *    responses:
 *      204:
 *        description: "The car model was removed."
 *      404:
 *        description: "Car model not found"
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 *      500:
 *        description: "Unexpected error."
 *        schema:
 *          $ref: '#/definitions/ErrorResponse'
 */
router.delete('/:brand/models/:model', modelController.removeCarModel);

export default router;
