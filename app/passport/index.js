'use strict';

import config from 'config';
import { default as localStrategy } from './local';
import { default as facebookStrategy } from './facebook';
import { default as twitterStrategy } from './twitter';

const socialKeys = config.socialKeys;

/**
 * Configure passport instance with different strategies.
 * @param passport
 */
export default (passport) => {

  localStrategy(passport);

  facebookStrategy(passport, {
    clientID: socialKeys.facebook.APP_ID,
    clientSecret: socialKeys.facebook.APP_SECRET,
    profileFields: ['id', 'name', 'email'],
    scope: ['email']
  });

  twitterStrategy(passport, {
    consumerKey: socialKeys.twitter.CONSUMER_KEY,
    consumerSecret: socialKeys.twitter.CONSUMER_SECRET,
    includeEmail: true
  });

}
