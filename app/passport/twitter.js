'use strict';

import mongoose from 'mongoose';
import shortid from 'shortid';
import TwitterStrategy from 'passport-twitter-token';
import getLogger from './../packages/logger';
import createUserAction from './../actions/createUser';

let logger = getLogger();
let User = mongoose.model('User');

/**
 * Strategy to authenticate based on Twitter access token.
 * @param passport The passport instance.
 * @param config   The customerKey and customerSecret keys.
 */
export default (passport, config) => {

  passport.use(new TwitterStrategy(config, (accessToken, refreshToken, profile, done) => {

    logger.info('Authorizing with Twitter');

    // Authorization granted.
    // Collect the user data provided after authorization.
    let userData = {
      name        : profile._json.name,
      email       : profile._json.email,
      password    : shortid(),
      provider    : 'twitter',
      twitter     : profile.id,
      confirmedAt : new Date()
    };

    // Look if the user already exists.
    // In that case assume a Login operation.
    return User.findOne({ $or: [{ email: userData.email }, { 'twitter': profile.id } ]}, (err, user) => {

      if (err) {
        logger.error('Error finding Twitter user', {
          accessToken: accessToken,
          refreshToken: refreshToken,
          userData: userData
        });
        logger.error(err);

        return done(err, null);
      }

      // The user already exists.
      if (user) {
        return done(err, user);
      }

      // Continue as a registration.
      // The user is not registered.
      return createUserAction(userData, (err, newUser) => done(err, newUser));

    });

  }));

};


