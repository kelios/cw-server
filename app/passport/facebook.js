'use strict';

import mongoose from 'mongoose';
import shortid from 'shortid';
import FacebookStrategy from 'passport-facebook-token';
import getLogger from './../packages/logger';
import createUserAction from './../actions/createUser';

let logger = getLogger();
let User = mongoose.model('User');

/**
 * Strategy to authenticate based on Facebook access token.
 * @param passport The passport instance.
 * @param config   The clientID and clientSecret keys.
 */
export default (passport, config) => {

  passport.use(new FacebookStrategy(config, (accessToken, refreshToken, profile, done) => {

    logger.info('Authorizing with Facebook');

    // Authorization granted.
    // Collect the user data provided after authorization.
    let userData = {
      name        : profile._json.first_name + ' ' + profile._json.last_name,
      email       : profile._json.email,
      password    : shortid(),
      provider    : 'facebook',
      facebook    : profile.id,
      confirmedAt : new Date()
    };

    return User.findOne({ $or: [{ email: userData.email }, { 'facebook': profile.id }] }, (err, user) => {

      if (err) {
        logger.error('Error finding Facebook user', {
          accessToken: accessToken,
          refreshToken: refreshToken,
          userData: userData
        });
        logger.error(err);

        return done(err, null);
      }

      // The user already exists.
      if (user) {
        return done(err, user);
      }

      // Continue as a registration.
      // The user is not registered.
      return createUserAction(userData, (err, newUser) => done(err, newUser));

    });

  }));

};


