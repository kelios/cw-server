'use strict';

import mongoose from 'mongoose';
import LocalStrategy from 'passport-local';
import getLogger from './../packages/logger';

let User = mongoose.model('User');
let logger = getLogger();

/**
 * Passport strategy for local authentication.
 * @param passport
 */
export default passport => {

  // Rewrite the fields
  let fields = {
    usernameField: 'login',
    passwordField: 'password'
  };

  // Configure passport with a local strategy.
  passport.use(new LocalStrategy(fields, function(login, password, done) {

    if (!login) {
      return done(null, false, { message: 'DNI or email is required' });
    }

    if (!password) {
      return done(null, false, { message: 'Password is required' });
    }

    // Matching the login with email or DNI.
    // Use provider local for local registration.
    User.findOne({ provider: 'local', '$or': [{ email: login }, { dni: login }]}, function(err, user) {

      if (err) {
        logger.error('Error login');
        logger.error(err);

        return done(err);
      }

      if (!user) {
        return done(null, false, {
          message: 'Unknown user.'
        });
      }

      // Check for password match.
      user.verifyPassword(password, function(err, valid) {

        if (err) {
          logger.error('Error login');
          logger.error(err);

          return done(err, false);
        }

        if (!valid) {
          return done(null, false, {
            message: 'Incorrect password'
          });
        }

        return done(null, user);

      });

    });

  }));

};
